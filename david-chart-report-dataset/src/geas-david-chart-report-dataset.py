#!/usr/bin/env python


#import ssl
#ssl._create_default_https_context = ssl._create_unverified_context
import sys
sys.path.append('../')

import logging
import traceback as tb
import suds.metrics as metrics
from geas_david_chart_report_dependencies import *
from suds import *
from suds.client import Client
from datetime import datetime
import csv
import argparse

valid_gene_identifier_types = [
        "AFFYMETRIX_3PRIME_IVT_ID",
        "AFFYMETRIX_EXON_ID",
        "AGILENT_CHIP_ID",
        "AGILENT_ID",
        "AGILENT_OLIGO_ID",
        "APHIDBASE_ID",
        "BEEBASE_ID",
        "BEETLEBASE_ID",
        "BGD_ID",
        "CGNC_ID",
        "CRYPTODB_ID",
        "DICTYBASE_ID",
        "ENSEMBL_GENE_ID",
        "ENSEMBL_TRANSCRIPT_ID",
        "ENTREZ_GENE_ID",
        "FLYBASE_GENE_ID",
        "GENBANK_ACCESSION",
        "GENOMIC_GI_ACCESSION",
        "GENPEPT_ACCESSION",
        "LOCUS_TAG",
        "MGI_ID",
        "MIRBASE_ID",
        "MRNA_GI_ACCESSION",
        "NASONIABASE_ID",
        "OFFICIAL_GENE_SYMBOL",
        "PROTEIN_GI_ACCESSION",
        "PSEUDOCAP_ID",
        "REFSEQ_MRNA",
        "REFSEQ_PROTEIN",
        "RGD_ID",
        "SGD_ID",
        "TAIR_ID",
        "UNIGENE",
        "UNIPROT_ACCESSION",
        "UNIPROT_ID",
        "VECTORBASE_ID",
        "WORMBASE_GENE_ID",
        "XENBASE_ID",
        "ZFIN_ID",
        "NA",
        ]

valid_categories =  [
        "BBID",
        "BIOCARTA",
        "COG_ONTOLOGY",
        "GOTERM_BP_FAT",
        "GOTERM_CC_FAT",
        "GOTERM_MF_FAT",
        "INTERPRO",
        "KEGG_PATHWAY",
        "OMIM_DISEASE",
        "PIR_SUPERFAMILY",
        "SMART",
        "SP_PIR_KEYWORDS",
        "UP_SEQ_FEATURE"
        ]

output_field_names = [ # the order changes the output file
        "Category",
        "Term",
        "Count",
        "%",
        "Pvalue",
        "Genes",
        "List Total",
        "Pop Hits",
        "Pop Total",
        "Fold Enrichment",
        "Bonferroni",
        "Benjamini",
        "FDR"
        ] 

def parse_arguments(args):
        DESCRIPTION='''Executes an enrichment analysis of gene expression data. This tool is a wrapper for the SOAP-based DAVID web service.
        '''
        parser = argparse.ArgumentParser(description=DESCRIPTION)
        parser.add_argument('--input', 
                required=True,
                type=argparse.FileType('r'), 
                nargs=1,
                help='An input TSV/CSV/CSV2 file. This file must have a column that will be used to find the gene IDs that will be looked up for the DAVID chart report. Use other parameters to set the used separator and quote character.')
        parser.add_argument('--column-name', 
                required=True,
                type=str, 
                nargs=1,
                help='The name of the column (contained in header) in which the gene ids will be found.')
        parser.add_argument('--output', 
                required=True,
                type=argparse.FileType('w'), 
                nargs=1,
                help='The TSV/CSV/CSV2 output file containing the chart report.')
        parser.add_argument('--email', 
                type=str, 
                nargs=1,
                required=True,
                help='The email address used to authenticate the user at DAVID Web Services.')
        parser.add_argument('--id-type', 
                type=str, 
                nargs=1,
                choices=valid_gene_identifier_types,
                required=True,
                help='The type of the gene identifiers. One of: [' +
                ', '.join(valid_gene_identifier_types) + ']')
        parser.add_argument('--threshold', 
                type=float, 
                nargs=1,
                default=0.1,
                help='Maximun EASE Score/p-value threshold.')
        parser.add_argument('--cutoff', 
                type=int, 
                nargs=1,
                default=2,
                help='Minimum number of term-associated genes necessary to that term to appear in results.')
        parser.add_argument('--categories-in-result', 
                type=str, 
                nargs='+',
                required=True,
                help='The categories to be present in the result. Valid categories: [' + ', '.join(valid_categories) + ']')
        parser.add_argument('--column-separator', 
                default='\t',
                type=str, 
                nargs=1,
                help='The character used to separate columns. Default: tab')
        parser.add_argument('--quote-char', 
                default='"',
                type=str, 
                nargs=1,
                help='The character used to quote the contents of a field. Default: double-quotes')
        parsed_args = parser.parse_args(args)
        return parsed_args


def get_input_gene_ids(csv_file, column, sep="\t", quote_char='"'):
    reader = csv.DictReader(csv_file,delimiter=sep,quotechar=quote_char)
    accumulator = []
    for row in reader:
        accumulator.append(row[column])
        print(row[column])
    
    return accumulator


def main(args = None):
        # PARSE ARGUMENTS
        parsed_args = vars(parse_arguments(args))
        print parsed_args
        auth_email = parsed_args['email'][0] 
        input_ids_file = parsed_args['input'][0]
        input_ids_column = parsed_args['column_name'][0]
        id_type =  parsed_args['id_type'][0]
        categories =  parsed_args['categories_in_result']
        threshold =  parsed_args['threshold'][0]
        cutoff =  parsed_args['cutoff'][0]
        output_file = parsed_args['output'][0]
        column_separator = parsed_args['column_separator'][0]
        quote_char = parsed_args['quote_char']

        input_ids =  get_input_gene_ids(input_ids_file,input_ids_column, sep=column_separator, quote_char=quote_char)
        input_ids = ",".join(input_ids)

        thd = 0.1
        ct = 2
        listName = 'make_up'
        listType = 0

        # WARM UP
        errors = 0
        setup_logging()
        logging.getLogger('suds.client').setLevel(logging.DEBUG)

        url = 'https://david.ncifcrf.gov/webservice/services/DAVIDWebService?wsdl'

        print 'url=%s' % url

        #
        # create a service client using the wsdl.
        #
        client = Client(url)
        client.wsdl.services[0].setlocation('https://david.ncifcrf.gov/webservice/services/DAVIDWebService.DAVIDWebServiceHttpSoap11Endpoint/')

        #authenticate user email
        client.service.authenticate(auth_email)

        #add a list
        print client.service.addList(input_ids, id_type, listName, listType)

        #print client.service.getDefaultCategoryNames()
        # setCategories
        categoryString = ",".join(categories)
        client.service.setCategories(categoryString)


        #getChartReport
        chartReport = client.service.getChartReport(threshold,cutoff)
        chartRow = len(chartReport)
        print 'Total chart records:',chartRow

        #parse and print chartReport
        #with open(output_file, 'w') as fOut:
        fOut = output_file


        writer = csv.DictWriter(output_file,output_field_names,delimiter=column_separator,quotechar=quote_char)
        writer.writeheader()

        for simpleChartRecord in chartReport:
            row = {
                    "Category" : str(simpleChartRecord.categoryName),
                    "Term" : str(simpleChartRecord.termName),
                    "Count" : str(simpleChartRecord.listHits),
                    "%" : str(simpleChartRecord.percent),
                    "Pvalue" : str(simpleChartRecord.ease),
                    "Genes" : str(simpleChartRecord.geneIds),
                    "List Total" : str(simpleChartRecord.listTotals),
                    "Pop Hits" : str(simpleChartRecord.popHits),
                    "Pop Total" : str(simpleChartRecord.popTotals),
                    "Fold Enrichment" : str(simpleChartRecord.foldEnrichment),
                    "Bonferroni" : str(simpleChartRecord.bonferroni),
                    "Benjamini" : str(simpleChartRecord.benjamini),
                    "FDR" : str(simpleChartRecord.afdr),
            }
            writer.writerow(row)
        print 'write file:', output_file, 'finished!'



if __name__ == '__main__':
    main()
