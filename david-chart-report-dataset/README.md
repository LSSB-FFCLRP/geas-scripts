# DAVID CHART REPORT client - Table file input dataset

A python command line tool for using the [Database for Annotation, Visualization and Integrated Discovery (DAVID) v6.8](https://david.ncifcrf.gov/).
This tool is a fork of the [provided Python Client v1.1](http://david.abcc.ncifcrf.gov/webservice/sample_clients/PythonClient-1.1.zip), customized to receive its arguments from the command line and read a TSV/CSV file to collect the genes of interest.

