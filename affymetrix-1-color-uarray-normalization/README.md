# Affymetrix One Color Microarray Normalization

More about CEL files in [here](https://www.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cel.html).


Currently, only the datasets for the platform `HG-U133A` are installed in the `preinstall` script.  In future, more platforms may be needed. Follow the example in the script, then build and update the installed package.

The installed script, using the `HG-U133A` platform, can be tested against the datasets in [GEO GSE1297](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE1297).

