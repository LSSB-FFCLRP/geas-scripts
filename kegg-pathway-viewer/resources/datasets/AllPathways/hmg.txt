path:hmg00010	Glycolysis / Gluconeogenesis - Hydra magnipapillata
path:hmg00020	Citrate cycle (TCA cycle) - Hydra magnipapillata
path:hmg00030	Pentose phosphate pathway - Hydra magnipapillata
path:hmg00040	Pentose and glucuronate interconversions - Hydra magnipapillata
path:hmg00051	Fructose and mannose metabolism - Hydra magnipapillata
path:hmg00052	Galactose metabolism - Hydra magnipapillata
path:hmg00053	Ascorbate and aldarate metabolism - Hydra magnipapillata
path:hmg00061	Fatty acid biosynthesis - Hydra magnipapillata
path:hmg00062	Fatty acid elongation - Hydra magnipapillata
path:hmg00071	Fatty acid degradation - Hydra magnipapillata
path:hmg00072	Synthesis and degradation of ketone bodies - Hydra magnipapillata
path:hmg00100	Steroid biosynthesis - Hydra magnipapillata
path:hmg00130	Ubiquinone and other terpenoid-quinone biosynthesis - Hydra magnipapillata
path:hmg00190	Oxidative phosphorylation - Hydra magnipapillata
path:hmg00230	Purine metabolism - Hydra magnipapillata
path:hmg00240	Pyrimidine metabolism - Hydra magnipapillata
path:hmg00250	Alanine, aspartate and glutamate metabolism - Hydra magnipapillata
path:hmg00260	Glycine, serine and threonine metabolism - Hydra magnipapillata
path:hmg00270	Cysteine and methionine metabolism - Hydra magnipapillata
path:hmg00280	Valine, leucine and isoleucine degradation - Hydra magnipapillata
path:hmg00290	Valine, leucine and isoleucine biosynthesis - Hydra magnipapillata
path:hmg00300	Lysine biosynthesis - Hydra magnipapillata
path:hmg00310	Lysine degradation - Hydra magnipapillata
path:hmg00330	Arginine and proline metabolism - Hydra magnipapillata
path:hmg00340	Histidine metabolism - Hydra magnipapillata
path:hmg00350	Tyrosine metabolism - Hydra magnipapillata
path:hmg00360	Phenylalanine metabolism - Hydra magnipapillata
path:hmg00380	Tryptophan metabolism - Hydra magnipapillata
path:hmg00400	Phenylalanine, tyrosine and tryptophan biosynthesis - Hydra magnipapillata
path:hmg00410	beta-Alanine metabolism - Hydra magnipapillata
path:hmg00430	Taurine and hypotaurine metabolism - Hydra magnipapillata
path:hmg00450	Selenocompound metabolism - Hydra magnipapillata
path:hmg00460	Cyanoamino acid metabolism - Hydra magnipapillata
path:hmg00480	Glutathione metabolism - Hydra magnipapillata
path:hmg00500	Starch and sucrose metabolism - Hydra magnipapillata
path:hmg00510	N-Glycan biosynthesis - Hydra magnipapillata
path:hmg00511	Other glycan degradation - Hydra magnipapillata
path:hmg00512	Mucin type O-Glycan biosynthesis - Hydra magnipapillata
path:hmg00514	Other types of O-glycan biosynthesis - Hydra magnipapillata
path:hmg00520	Amino sugar and nucleotide sugar metabolism - Hydra magnipapillata
path:hmg00531	Glycosaminoglycan degradation - Hydra magnipapillata
path:hmg00532	Glycosaminoglycan biosynthesis - chondroitin sulfate / dermatan sulfate - Hydra magnipapillata
path:hmg00533	Glycosaminoglycan biosynthesis - keratan sulfate - Hydra magnipapillata
path:hmg00534	Glycosaminoglycan biosynthesis - heparan sulfate / heparin - Hydra magnipapillata
path:hmg00561	Glycerolipid metabolism - Hydra magnipapillata
path:hmg00562	Inositol phosphate metabolism - Hydra magnipapillata
path:hmg00563	Glycosylphosphatidylinositol(GPI)-anchor biosynthesis - Hydra magnipapillata
path:hmg00564	Glycerophospholipid metabolism - Hydra magnipapillata
path:hmg00565	Ether lipid metabolism - Hydra magnipapillata
path:hmg00590	Arachidonic acid metabolism - Hydra magnipapillata
path:hmg00592	alpha-Linolenic acid metabolism - Hydra magnipapillata
path:hmg00600	Sphingolipid metabolism - Hydra magnipapillata
path:hmg00603	Glycosphingolipid biosynthesis - globo series - Hydra magnipapillata
path:hmg00604	Glycosphingolipid biosynthesis - ganglio series - Hydra magnipapillata
path:hmg00620	Pyruvate metabolism - Hydra magnipapillata
path:hmg00630	Glyoxylate and dicarboxylate metabolism - Hydra magnipapillata
path:hmg00640	Propanoate metabolism - Hydra magnipapillata
path:hmg00650	Butanoate metabolism - Hydra magnipapillata
path:hmg00670	One carbon pool by folate - Hydra magnipapillata
path:hmg00730	Thiamine metabolism - Hydra magnipapillata
path:hmg00740	Riboflavin metabolism - Hydra magnipapillata
path:hmg00750	Vitamin B6 metabolism - Hydra magnipapillata
path:hmg00760	Nicotinate and nicotinamide metabolism - Hydra magnipapillata
path:hmg00770	Pantothenate and CoA biosynthesis - Hydra magnipapillata
path:hmg00780	Biotin metabolism - Hydra magnipapillata
path:hmg00785	Lipoic acid metabolism - Hydra magnipapillata
path:hmg00790	Folate biosynthesis - Hydra magnipapillata
path:hmg00830	Retinol metabolism - Hydra magnipapillata
path:hmg00860	Porphyrin and chlorophyll metabolism - Hydra magnipapillata
path:hmg00900	Terpenoid backbone biosynthesis - Hydra magnipapillata
path:hmg00910	Nitrogen metabolism - Hydra magnipapillata
path:hmg00920	Sulfur metabolism - Hydra magnipapillata
path:hmg00970	Aminoacyl-tRNA biosynthesis - Hydra magnipapillata
path:hmg00980	Metabolism of xenobiotics by cytochrome P450 - Hydra magnipapillata
path:hmg00982	Drug metabolism - cytochrome P450 - Hydra magnipapillata
path:hmg00983	Drug metabolism - other enzymes - Hydra magnipapillata
path:hmg01040	Biosynthesis of unsaturated fatty acids - Hydra magnipapillata
path:hmg01100	Metabolic pathways - Hydra magnipapillata
path:hmg01200	Carbon metabolism - Hydra magnipapillata
path:hmg01210	2-Oxocarboxylic acid metabolism - Hydra magnipapillata
path:hmg01212	Fatty acid metabolism - Hydra magnipapillata
path:hmg01220	Degradation of aromatic compounds - Hydra magnipapillata
path:hmg01230	Biosynthesis of amino acids - Hydra magnipapillata
path:hmg02010	ABC transporters - Hydra magnipapillata
path:hmg03008	Ribosome biogenesis in eukaryotes - Hydra magnipapillata
path:hmg03010	Ribosome - Hydra magnipapillata
path:hmg03013	RNA transport - Hydra magnipapillata
path:hmg03015	mRNA surveillance pathway - Hydra magnipapillata
path:hmg03018	RNA degradation - Hydra magnipapillata
path:hmg03020	RNA polymerase - Hydra magnipapillata
path:hmg03022	Basal transcription factors - Hydra magnipapillata
path:hmg03030	DNA replication - Hydra magnipapillata
path:hmg03040	Spliceosome - Hydra magnipapillata
path:hmg03050	Proteasome - Hydra magnipapillata
path:hmg03060	Protein export - Hydra magnipapillata
path:hmg03410	Base excision repair - Hydra magnipapillata
path:hmg03420	Nucleotide excision repair - Hydra magnipapillata
path:hmg03430	Mismatch repair - Hydra magnipapillata
path:hmg03440	Homologous recombination - Hydra magnipapillata
path:hmg03450	Non-homologous end-joining - Hydra magnipapillata
path:hmg03460	Fanconi anemia pathway - Hydra magnipapillata
path:hmg04070	Phosphatidylinositol signaling system - Hydra magnipapillata
path:hmg04080	Neuroactive ligand-receptor interaction - Hydra magnipapillata
path:hmg04120	Ubiquitin mediated proteolysis - Hydra magnipapillata
path:hmg04122	Sulfur relay system - Hydra magnipapillata
path:hmg04130	SNARE interactions in vesicular transport - Hydra magnipapillata
path:hmg04140	Regulation of autophagy - Hydra magnipapillata
path:hmg04141	Protein processing in endoplasmic reticulum - Hydra magnipapillata
path:hmg04142	Lysosome - Hydra magnipapillata
path:hmg04144	Endocytosis - Hydra magnipapillata
path:hmg04145	Phagosome - Hydra magnipapillata
path:hmg04146	Peroxisome - Hydra magnipapillata
path:hmg04150	mTOR signaling pathway - Hydra magnipapillata
path:hmg04310	Wnt signaling pathway - Hydra magnipapillata
path:hmg04320	Dorso-ventral axis formation - Hydra magnipapillata
path:hmg04330	Notch signaling pathway - Hydra magnipapillata
path:hmg04340	Hedgehog signaling pathway - Hydra magnipapillata
path:hmg04350	TGF-beta signaling pathway - Hydra magnipapillata
path:hmg04512	ECM-receptor interaction - Hydra magnipapillata
path:hmg04630	Jak-STAT signaling pathway - Hydra magnipapillata
