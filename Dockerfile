FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

RUN apt-get update 
RUN apt-get install -y software-properties-common
RUN apt-get update 
RUN apt-get install -y apt-utils

RUN apt-get install -y r-base=3.6.3-2
RUN apt-get install -y curl 
RUN apt-get install -y libcurl4-openssl-dev
# DeSeq2
RUN apt-get install -y libxml2-dev libc6-dev 
RUN apt-get install -y libssl-dev libc6-dev

# DAVID-Chart-Report
RUN apt-get install -y python python-setuptools 
RUN python /usr/lib/python2.7/dist-packages/easy_install.py suds-community

# Install packages
RUN mkdir -p /dist
COPY ./dist/ /dist
RUN find dist -iname "*.deb" -exec dpkg -i {} \; 
