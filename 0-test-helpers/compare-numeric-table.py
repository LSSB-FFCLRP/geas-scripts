#!/usr/bin/env python3

########################################################################
# 

import sys
import csv
import argparse
import math


def parse_arguments(args):
    DESCRIPTION = ''' Compares two T/CSV files for similar values. The tool does not compare cell ordering, but tries to use row and column identifiers to compare cell contents within a maximun tolerance value. Returns 0 if similar and 1 otherwise.'''
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('file1',
            type = argparse.FileType('r'),
#            required = True,
            nargs=1,
            help = 'The first file to compare')
    parser.add_argument('file2',
            type = argparse.FileType('r'),
#            required = True,
            nargs=1,
            help = 'The second file to compare')
    parser.add_argument('index',
            type = str,
#            required = True,
            nargs=1,
            help = 'The name of the column used as index for comparison. The rows in both  files will be paired using this identifier.')
    parser.add_argument('tolerance',
            type = float,
#            required = True,
            nargs=1,
            help = 'The second file to compare')
    parser.add_argument('--sep',
            default = '\t',
            type = str,
#            required = True,
            nargs=1,
            help = 'The column separator character.')
    return parser.parse_args(args)


def load_table(table_file, column, sep="\t", quote_char='"'):
    reader = csv.DictReader(table_file,delimiter=sep,quotechar=quote_char)
    accumulator = {}
    for row in reader:
        accumulator[row[column]] = row
    
    return accumulator

def compare_numeric_tables(table1, table2,tolerance):
    valid = True
    for identifier in table1.keys():
        columns = list(table1[identifier].keys())
        for column_name in [x for x in columns if x != 'ID']:
            value1 = float(table1[identifier][column_name])
            value2 = float(table2[identifier][column_name])
            
            # comparison rules:
            if math.isnan(value1) and math.isnan(value2):
                continue

            is_close = math.isclose(value1,value2,abs_tol=tolerance)
            if is_close:
                continue

            print(table1[identifier])
            print(table2[identifier])
            valid = False
            break
    return valid

def main(args = None):
    parsedArgs = vars(parse_arguments(args))
    file1 = parsedArgs['file1'][0]
    file2 = parsedArgs['file2'][0]
    index = parsedArgs['index'][0]
    tolerance = parsedArgs['tolerance'][0]
    separator = parsedArgs['sep'][0]
    
    table1 = load_table(file1,index,sep=separator)
    table2 = load_table(file2,index,sep=separator)

    #print(table1)
    #print(table2)

    valid = compare_numeric_tables(table1,table2,tolerance)
    sys.exit(0 if valid else 1)

if __name__ == '__main__':
    main()
