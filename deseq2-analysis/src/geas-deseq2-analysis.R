#!/usr/bin/env Rscript

############################################################################
# DESeq2 analysis script used for:
# - RnaSeqDifferentialAnalysisService
############################################################################

library("argparser");
library("DESeq2");

# Create a command line parser
p <- arg_parser("Execute a DESeq2-based analysis");


p <- add_argument(p, "--reference-samples", 
	help="REQUIRED. Reference samples' files.",
	nargs=Inf, 
	type="character");
p <- add_argument(p, "--target-samples", 
	help="REQUIRED. Target samples' files.",
	nargs=Inf, 
	type="character");
#p <- add_argument(p, "--directory", 
#	help="Base directory for files.",
#	nargs=1, 
#	type="character");
#p <- add_argument(p, "--input-files", 
#	help="1-N input files",
#	nargs=Inf, 
#	type="character");
p <- add_argument(p, "--output-file", 
	help="REQUIRED. Output file",
	nargs=1, 
	type="character");
p <- add_argument(p, "--threshold", 
	help="REQUIRED. Filtering threshold for p-value or false dicovery rate",
	nargs=1, 
	type="numeric");
p <- add_argument(p, "--cutoff", 
	help="REQUIRED. Cutoff (log2-based) for filtering the fold change.",
	nargs=1, 
	type="numeric")
p <- add_argument(p, "--by-false-discovery-rate",
	help="Flag. Filter significant genes by false discovery rate. Default: filter by p-value", 
	flag=TRUE);
p <- add_argument(p, "--separator", 
	help="Column-separator character for input/output dataset files. Default: tab.",
	nargs=1, 
	type="character",
	default="\t");

# get the arguments
args <- parse_args(p);

print(args)


# Get command line arguments
#rnaSeqFiles <- args$input_files; # an array
referenceSamples <- args$reference_samples;
targetSamples <- args$target_samples;
outputFile <- args$output_file;
threshold <- args$threshold;
cutoff <- args$cutoff;
fdrAdjust <- args$by_false_discovery_rate;
separator <- args$separator;

rnaSeqFiles = c(referenceSamples,targetSamples);

directory.separator = "/";
        
# PROGRAM ------------------------------------------------------------------------------------        
# Load DESeq2 package
library(DESeq2,quietly=TRUE);
        

sampleConditions <- character();

for(file in rnaSeqFiles) {
	if(file %in% referenceSamples){ #file in reference
		sampleConditions = c(sampleConditions,"reference");
	} else if (file %in% targetSamples) { #file in target
		sampleConditions = c(sampleConditions,"target");
	} else {
		stop(paste("File",file," is neither as reference or target"));		
	}
}

        
# Store experimental Conditions
experimentalConditions <- sampleConditions;
        
# Create list to store names (labels) and data from input files
data = labelList = list();

# Read input files and create a countTable unifying all files (each row representing a gene and each column representing a file)
for(file in rnaSeqFiles){
	fullPath = file;
	data[[fullPath]] = read.delim(fullPath, stringsAsFactors=FALSE);
	labelList[[fullPath]] = as.character(data[[fullPath]][,1]);
}

labels 		<- unique(unlist(labelList));
countTable 	<- matrix(0, length(labels), length(rnaSeqFiles));
rownames(countTable) <- unique(unlist(labelList));
colnames(countTable) <- rnaSeqFiles;

for(file in rnaSeqFiles) {
	aa <- match(labelList[[file]],labels);
	countTable[aa,file] = data[[file]][,2];
}
        
# Create dataframe of columns information (experimental conditions)
sampleInfo = data.frame(row.names=rnaSeqFiles,condition=experimentalConditions);
        
# Create a DESeqDataSet object
dataSet = DESeqDataSetFromMatrix(countData=countTable,colData=sampleInfo,design=~condition);
        
# Put the reference condition as the first level in the DESeqDataSet (needed to correctly perform the analysis)
dataSet$condition = relevel(dataSet$condition, "reference");
        
# Perform differential analysis in DESeqDataSet object
dataSet = DESeq(dataSet);
        
# Get results of analysis
result = results(dataSet);
        
# Exclude NA values
naResult = na.exclude(result);
        
# Filter for significant genes according to some chosen threshold for the false dicovery rate (FDR) or p-value
# Filter log2 fold change according to some chosen cutoff
# And order differentially expressed genes (adjusted p-value or p-value)
if(fdrAdjust) { # filter by padj
	filteredResult = subset(naResult,padj < threshold);
	filteredResult2 = subset(filteredResult,abs(log2FoldChange) > cutoff);
	orderedResult = filteredResult2[order(filteredResult2$padj),];	
} else { # filter by false discovery rate
	filteredResult = subset(naResult, pvalue < threshold);
	filteredResult2 = subset(filteredResult,abs(log2FoldChange) > cutoff);
	orderedResult = filteredResult[order(filteredResult$pvalue), ];
}

# Include first column label (gene.id)
finalResult = data.frame(row.names=NULL,gene.id=rownames(orderedResult),orderedResult[,1:6]);
        
# Write final result to output file
write.table(finalResult,file=outputFile,sep=separator,row.names=FALSE,quote=FALSE);
