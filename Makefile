TEST_FOLDER=test-suite
LABEL=1.0
IMAGE_NAME=geas-scripts
TAG_BASE=cawal
TAG=${TAG_BASE}${IMAGE_NAME}:${LABEL}



docker-image: deb-all
	mkdir -p dist
	find -iname "*.deb" -exec cp {} dist/ \;
	docker build -t "${IMAGE_NAME}:${LABEL}" .
	docker tag "${IMAGE_NAME}:${LABEL}" "${TAG}"
	#docker push "${TAG}"

install-deb: deb-all
	@echo ---------- INSTALLING DEB PACKAGES -----------
	find . -iname "*.deb" -print -exec sudo dpkg -i {} \;	
	sudo apt-get -f install

deb-all: deb-affymetrix-1-color-uarray-normalization deb-agilent-1-color-uarray-normalization deb-cluster deb-david-chart-report deb-david-chart-report-dataset deb-deseq2-analysis deb-gene-set-enrichment deb-gprofiler-enrichment-analysis deb-kegg-pathway-viewer deb-micro-genepix-norm deb-one-color-uarray-fold-change deb-one-color-uarray-t-test deb-two-color-uarray-fold-change deb-two-color-uarray-t-test 

tests:
	(cd ${TEST_FOLDER}; make all) 


deb-affymetrix-1-color-uarray-normalization:
	cd affymetrix-1-color-uarray-normalization; make deb

deb-agilent-1-color-uarray-normalization:
	cd agilent-1-color-uarray-normalization; make deb

deb-cluster:
	cd cluster; make deb

deb-david-chart-report:
	cd david-chart-report; make deb

deb-david-chart-report-dataset:
	cd david-chart-report-dataset; make deb

deb-deseq2-analysis:
	cd deseq2-analysis; make deb

deb-gene-set-enrichment:
	cd gene-set-enrichment; make deb

deb-gprofiler-enrichment-analysis:
	cd gprofiler-enrichment-analysis; make deb

deb-kegg-pathway-viewer:
	cd kegg-pathway-viewer; make deb

deb-micro-genepix-norm: 
	cd micro-genepix-norm; make deb

deb-one-color-uarray-fold-change:
	cd one-color-uarray-fold-change; make deb

deb-one-color-uarray-t-test:
	cd one-color-uarray-t-test; make deb

deb-two-color-uarray-fold-change:
	cd two-color-uarray-fold-change; make deb

deb-two-color-uarray-t-test:
	cd two-color-uarray-t-test; make deb



