# DAVID CHART REPORT client

A python command line tool for using the [Database for Annotation, Visualization and Integrated Discovery (DAVID) v6.8](https://david.ncifcrf.gov/).
This tool is a fork of the [provided Python Client v1.1](http://david.abcc.ncifcrf.gov/webservice/sample_clients/PythonClient-1.1.zip), customized to receive its arguments from the command line.

## Testing

First, register an email at [the DAVID Webservice Registration page](https://david.ncifcrf.gov/webservice/register.htm).

Then, the chart report client can be tested using the following command line call:

```sh
EMAIL="<put registered email here>"
GENE_IDS="1112_g_at,1331_s_at,1355_g_at"
GENE_IDENTIFIER_TYPE="AFFYMETRIX_3PRIME_IVT_ID"
CATEGORIES_IN_RESULT="BBID,BIOCARTA,COG_ONTOLOGY,GOTERM_BP_FAT,GOTERM_CC_FAT,GOTERM_MF_FAT,INTERPRO,KEGG_PATHWAY,OMIM_DISEASE,PIR_SUPERFAMILY,SMART,SP_PIR_KEYWORDS,UP_SEQ_FEATURE"
COUNT="2"
THRESHOLD="2.0"
OUTPUT="output.tsv"
geas-david-chart-report.py "${EMAIL}" "${GENE_IDS}" "${GENE_IDENTIFIER_TYPE}" "${CATEGORIES_IN_RESULT}" "${THRESHOLD}" "${COUNT}" "${OUTPUT}" 
"BBID,BIOCARTA,COG_ONTOLOGY,GOTERM_BP_FAT,GOTERM_CC_FAT,GOTERM_MF_FAT,INTERPRO,KEGG_PATHWAY,OMIM_DISEASE,PIR_SUPERFAMILY,SMART,SP_PIR_KEYWORDS,UP_SEQ_FEATURE" "2.0" "2" "teste.tsv"
```

The tool should return a zero exit code and produce a file in the directory.
