## GEAS services update 

### Analysis Services 

| :---                                               | :---                                          | :--- | :--- | :---    | :--- | :--- |
| old service                                        | new service                                   | tool | test | service | test | docs |
| :---                                               | :---                                          | :--- | :--- | :---    | :--- | :--- |
| DAVID-REST Service                                 | david-chart-report                            | V    | V    | V       | V    |      |
| DAVID-REST Service                                 | david-chart-report-by-dataset                 | V    | V    | V       | V    |      |
| MicroAffyNorm Service                              | affymetrix-one-color-microarray-normalization | V    | V    | V       | V    |      |
| MicroAgilentNorm Service                           | agilent-one-color-microarray-normalization    | V    | V    | V       |      |      |
| EnrichmentAnalysis Service                         | enrichment-analysis                           | V    | V    | V       | V    |      |
| KeggPathwayViewer Service                          | kegg-pathway-viewer                           | V    |      | V       |      |      |
| MicroGenepixNorm Service                           | genepix-microarray-normalization              | V    |      | V       |      |      |
| MicroOneDifferentialAnalysis Service - fold change | one-color-microarray-fold-change              | V    |      | V       |      |      |
| MicroOneDifferentialAnalysis Service - t-test      | one-color-microarray-t-test                   | V    |      | V       |      |      |
| MicroTwoDifferentialAnalysis Service - fold change | two-color-microarray-fold-change              | V    |      | V       |      |      |
| MicroTwoDifferentialAnalysis Service - t-test      | two-color-microarray-t-test                   | V    |      | V       |      |      |
| MicroHCluster Service                              |                                               | +    |      | V?      |      |      |
| MicroKCluster Service                              |                                               | +    |      | V?      |      |      |
| MicroHClusterViewer Service                        |                                               | ++   |      | ??      |      |      |
| RnaSeqDifferentialAnalysis Service                 | deseq2-analysis                               | V    | ...  | a       |      |      |
| GeneSetEnrichmentAnalysis Service                  | gene-set-enrichment                           | V    | ...  | a,b     |      |      |

a: Precisa de um novo StringListManipulator ToFlag : Boolean -> string in {ifTrueString,ifFalseString},
b: Poderia incluir já uma constraint, dado que algumas strings possuem valores enumeráveis
V?: Possui diferentes métodos de execução. Acho que um é um método com resultado completo. Imeplementei este. Estará correto?
??: Precisa de uma implementação nova do Treeview?
+ Usa o software [Cluster](http://bonsai.hgc.jp/~mdehoon/software/cluster/software.htm#ctv)
++ Usa o software [Java TreeView](http://jtreeview.sourceforge.net/)


## Available Adaptation Services:

| :---                        | :---        | :--- | :--- | :---    | :--- | :--- |
| old service                 | new service | tool | test | service | test | docs |
| :---                        | :---        | :--- | :--- | :---    | :--- | :--- |
| Adaptation Service 1  (AS1) |             |      |      |         |      |
| Adaptation Service 2  (AS2) |             |      |      |         |      |
| Adaptation Service 3  (AS3) |             |      |      |         |      |
| Adaptation Service 4  (AS4) |             |      |      |         |      |
| Adaptation Service 5  (AS5) |             |      |      |         |      |
| Adaptation Service 6  (AS6) |             |      |      |         |      |

## Sample Analysis Scenarios:

| :---                     | :---        | :--- | :--- | :---    | :--- | :--- |
| old service              | new service | tool | test | service | test | docs |
| :---                     | :---        | :--- | :--- | :---    | :--- | :--- |
| Microarray Data Analysis |             |      |      |         |
| RNA-Seq Data Analysis    |             |      |      |         |
