# GEAS-RIM script dependencies

A number of Python2/R scripts used in GEAS web services. These scripts were extracted from the GEAS implementation received from Gabriela Guardia.
Primarily used in an Ubuntu (18.04) machine. MacOS/Windows installation procedures may be defined in the future. 


## Cloning the repository

Don't forget to `init` and `update` the test data submodule!

```bash
git submodule init
git submodule update
```

## Building and installing

The root makefile can be used to compile and install all projects.
The `install-deb` target will compile the DEB package for each project and then install it using DPKG, also trying to install their dependencies using APT-GET.
Execute the next line for this 

```bash
make install-deb
```

Alternatively, the root folder of each package contains a `Makefile` that allow to perform different actions on it.
For creating the DEB package using these `Makefile`s, execute in the the package directory and execute:

```bash
make deb
```

A DEB file will be created in the `dist/deb/` folder of the package.


## Contributing

### Dependency management for scripts:

Packages (mainly, the ones with R scripts) have two sets of dependencies: 

1. DEB dependencies that must be checked before the deployment;
2. Third-party R packages that are installed during the deployment.

DEB dependencies must be installed previously using `apt`  or `dpkg`. These dependencies are listed in the `resources/DEBIAN/control` file in each subproject. Non-Ubuntu-users may use this list to manually install the necessary packages manually.
Third-party R packages are automatically installed by the `resources/DEBIAN/preinst` script. Non-Ubuntu-users may use the `preinst`script as guide while manually installing these packages.


### Creating tests for scripts

Usually, a test will need a number of resources:

- One or more input datasets for the script;
- One or more output datasets created after script execution; 
- The command issued to execute the script.
- A validation mechanism, sometimes using one or more expected datasets compared against the dataset produced during tool execution.

Input datasets shall be included in a separated git repository in order to maintain this (development) repository lean.
The git submodule in `test-resources` is used for storing these datasets.
As bioinfomatics dataset usually are big and need a lot of storage resources, try to use only minimum representative input datasets if possible and avoid file duplication within the repositories.

Output datasets may be created anywhere, but MUST NOT be commited to any repository.
The best method to deal with them is to have a `test-sandbox/` folder.
This folder must be used as the target for the creation of any output dataset or test files.
Further, the tests should clean the `test-sandbox/` folder contents after the successful execution.
In order to avoid any accidental dataset commits, a `.gitignore` file should be created within the `test-sandbox/` folder containg the follwing lines: 

```
*
!gitignore
```

The commands issued to execute the test and validate the results must be created in the tool's `Makefile`.
Each test should be created as a separated make's target.
This target should be named with a ASCII string starting with `test-` followed by a number or some more meaningful name. 
A main make's target named `tests` must be created to aggregate all tests to be runned.
Test examples can be found at `./affymetrix-1-color-uarray-normalization/`.

Finally, expected result datasets used to comparison with produced datasets shold also be commited in the separated dataset repository.
Again, if possible, try to use only small representative dataset examples.
Remember to update and commit the dataset repository accordingly.

