FROM ubuntu:bionic

RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
RUN apt-get update 
RUN apt-get install -f r-base 
RUN apt-get install -f curl
RUN apt-get install libcurl4-openssl-dev
RUN apt-get install libxml2-dev
RUN apt-get install libssl-dev


COPY deb /

RUN find deb -exec dpkg -i {} \; 

